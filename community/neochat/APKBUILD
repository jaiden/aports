# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=neochat
pkgver=23.04.3
pkgrel=3
pkgdesc="A client for Matrix, the decentralized communication protocol"
url="https://invent.kde.org/network/neochat/"
# armhf blocked by extra-cmake-modules
# riscv64 and s390x blocked by qqc2-desktop-style
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-or-later AND GPL-3.0-only AND GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kirigami-addons
	kirigami2
	kitemmodels
	kquickimageeditor
	qqc2-desktop-style
	qt5-qtquickcontrols
	"
makedepends="
	cmark-dev
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	kitemmodels-dev
	knotifications-dev
	kquickimageeditor-dev
	libquotient-dev
	qcoro-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qtkeychain-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	sonnet-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/network/neochat.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/neochat-$pkgver.tar.xz
	0001-Pick-libQuotient-0.8-fixes.patch
	0002-Always-enable-E2EE-in-libQuotient.patch
	"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e453c3bab98a45fe1d1d7139b574ff25e6d7a78749b38966eec17fa67ff366eea955aa82cde0743135c8ac35ca218a761abced055743545db760d67849e3a96  neochat-23.04.3.tar.xz
e19559527d3d31e4e2b3e6846c3c9f9e27269a564939085a7e6a4763519199254ebd6874b279427d52200331d70a3351b600ccc08eb4214bd3bde70dc6277ebf  0001-Pick-libQuotient-0.8-fixes.patch
c7fc0815162e0b352a28ede871c01c0dca48ca690606ca0932898032f9ff439b74d5cdbd51fca43050d19d8d6430fd189f89d87d514862dda56a9fb77c4156ec  0002-Always-enable-E2EE-in-libQuotient.patch
"
