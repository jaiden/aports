# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kde-inotify-survey
pkgver=23.04.3
pkgrel=1
pkgdesc="Tooling for monitoring inotify limits and informing the user when they have been or about to be reached"
url="https://invent.kde.org/system/kde-inotify-survey"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND (GPL-2.0-only OR GPL-3.0-only)"
# zstd is purely used to unpack the source archive
makedepends="
	extra-cmake-modules
	kauth-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
_repo_url="https://invent.kde.org/system/kde-inotify-survey.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-inotify-survey-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f42bb2be585ac4e25d162e8752b5e756ef735a533cb4ff9efa464a7ea36c5c08e3cf2d53aa58c2f281b8fc2f8dbc9127e51287e78db154b0ad51af8176030686  kde-inotify-survey-23.04.3.tar.xz
"
