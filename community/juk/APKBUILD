# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=juk
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://juk.kde.org/"
pkgdesc="A jukebox, tagger and music collection manager"
license="GPL-2.0-or-later"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	taglib-dev
	"
_repo_url="https://invent.kde.org/multimedia/juk.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/juk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
39e864c6a42c251d141c38266d5e320ef8885bdee872609e11d65b080125293b442cb60145b14c63a2a33b16609b0ac8f59458e8ffa7a6da4f0e013dfdb1f146  juk-23.04.3.tar.xz
"
