# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=247
pkgrel=0
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	"
makedepends="
	py3-docutils
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	py3-pytest-xdist
	unzip
	"
subpackages="$pkgname-pyc"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"
options="!check" # need update for new file(1)

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# html test fails
	PYTHONDONTWRITEBYTECODE=1 \
	.testenv/bin/python3 -m pytest -n auto -k 'not test_diff'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e78474d43f8d8549e4c1a576cd8c6aa195dc3a04cc819e39eb809d063dc97737b26ad06f162e4a82139e8adc591795a2f4578a686e67aa405137136315c5beb4  diffoscope-247.tar.gz
"
